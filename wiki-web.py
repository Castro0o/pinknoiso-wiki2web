#! /usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET
import html5lib, pprint, re, mwclient, urllib
from modules import pandoc2html, write_html_file, mw_cats, mw_page_imgsurl, remove_cats, replace_video
from argparse import ArgumentParser


p = ArgumentParser()
p.add_argument("--host", default="wiki.pinknoi.so")
p.add_argument("--path", default="/", help="nb: should end with /")
p.add_argument("--category", "-c", nargs="*", default=[["Blog", "Works", "Neighbors", "Cookbook", "Me"]], action="append", help="category to query, use -c Foo -c Bar ") # Work
args = p.parse_args()
#print 'args', args


def lastitem(t):
    return t[-1]

def parse_page(mwtext):
    if re.match(u'\{\{\Page', mwtext):
        pagedict = {}
        template, text = (re.findall(u'\{\{Page(.*?)\}\}(.*)', mwtext, re.DOTALL))[0]
#        text = text.replace('\n', '')
        text = remove_cats(text)
        text = replace_video(text)
        pagedict['Text']=(pandoc2html(text))#.replace('\n', '')

        keyval = re.findall(u'\|(.*?)\=(.*?\n)', template, re.DOTALL)# page metadata (template)
        for pair in keyval:
            key = pair[0].replace('\n', '')
            val = (pair[1]).replace('\n', '')
            if 'Page type' in key:
                pagedict['Type']=val
            elif 'Date' in key:
                pagedict['Date']=val
        return pagedict


def create_page(mwpage):
    pagedict = parse_page(mwpage.text())
#    print 'mwpage', mwpage
    pagedict['Title'] = mwpage.name
    if 'Date' in pagedict.keys():
        pagedict['File'] = ("{}-{}-{}.html".format(  pagedict['Date'].replace('.',''), 
                                                     pagedict['Type'], 
                                                     pagedict['Title'])).replace(' ','')
    else: #if no Date it is a Category Page or Page W/Out date
        if 'Category:' in pagedict['Title']: # (category) Main page
            pagedict['File'] = ("{}.html".format(pagedict['Title'])).replace(' ','').replace('Category:', 'Main-')
            pagedict['Title'] = pagedict['Title'].replace('Category:', '')
            pagedict['Type'] = 'Category Page' 
            print pagedict['Title']
            if pagedict['Title'] == u'Home':
                print 'Found', pagedict['File']
                pagedict['File'] = 'index.html'
        else: # normal page without dare ''
            pagedict['File'] = urllib.quote("{}-{}.html".format(pagedict['Type'], pagedict['Title'])).replace(' ','')

    # if pagedict['File'] is u'Main-Home.html':
    #     print 'Found', pagedict['File']
    #     pagedict['File'] = 'index.html'
        
    imgs = mw_page_imgsurl(mwpage)
    if imgs :
        pagedict['Imgs'] = imgs # get images full url

    return pagedict

def template_fill(page, templatebase, indexdict):
    if page in indexdict['Pages'].keys():
        pagedict = indexdict['Pages'][page] 
    elif page in indexdict['Cats'].keys():
        pagedict = indexdict['Cats'][page] 

    page_tree = html5lib.parse(templatebase, namespaceHTMLElements=False)
    page_content = page_tree.find('.//div[@id="content"]')
    page_text  = ET.fromstring('<div>'+pagedict['Text'].encode('utf-8')+'</div>') 
    page_content.extend(page_text) 
    page_title = page_tree.find('.//h2[@id="title"]')
    page_title.text=pagedict['Title']
    page_title = page_tree.find('.//title')
    page_title.text=pagedict['Title']
    page_title = page_tree.find('.//meta[@property="og:title"]')
    page_title.set('content', pagedict['Title'])

    if 'Date' in pagedict.keys():
        page_date = page_tree.find('.//h4[@class="year"]')
        page_date.text=pagedict['Date']        

    # if 'Imgs' in pagedict.keys():
    #     imgs = page_tree.findall('.//img') 
    #     for img in imgs:
    #         img_src = (('File:'+img.get('src')).capitalize()).decode('utf-8')
    #         if img_src in pagedict['Imgs'].keys(): 
    #             url = pagedict['Imgs'][img_src]
    #             img.set('src', url)

    figures = page_tree.findall('.//figure') 
    for figure in figures:
        img = figure.find('.//img')            
        img_src = (('File:'+img.get('src')).capitalize()).decode('utf-8')
        if img_src in pagedict['Imgs'].keys(): 
            url = pagedict['Imgs'][img_src]
            img.set('src', url)

        figcaption = figure.find('.//figcaption')
        figcaption_text = figcaption.text
        if (figcaption_text).lower() in (img_src).lower():
            figure.remove(figcaption)    

    # category index
    el_menu = page_tree.find('.//ul[@class="menu"]') 
    cats = indexdict['Cats'].keys()    
    cats.remove(u'Home')
    cats.remove(u'Blog')
    cats.remove(u'Works')
    cats.insert(0, u'Home')
    cats.insert(1, u'Works')
    cats.insert(2, u'Blog')
    for key in  cats:
        li = ET.SubElement(el_menu, 'li', attrib={'class':'menu'})
        li_a = ET.SubElement(li,'a',attrib={'href':indexdict['Cats'][key]['File']})
        li_a.text = indexdict['Cats'][key]['Title']        
        
    # pages index (category members)
    memberpages = indexdict['Cats'][pagedict['Type']]['MemberPages'] #files part of type
    memberfiles = [(member, indexdict['Pages'][member]['File']) for member in memberpages] #corresponding files
    memberfiles = sorted(memberfiles, key=lastitem, reverse=True)
    memberpages = [member[0] for member in memberfiles] # return only the key, sorted according to filename
    el_catmemberpages = page_tree.find('.//ul[@class="catmembers"]')   
    for page in memberpages:
        # exlclude Category pages (same Type as Name)
        if indexdict['Pages'][page]['Type'] != indexdict['Pages'][page]['Title']: 
            li_page = ET.SubElement(el_catmemberpages, 'li')
            li_page_a = ET.SubElement(li_page,'a',attrib={'href':urllib.quote(indexdict['Pages'][page]['File']) })
            li_page_a.text = indexdict['Pages'][page]['Title']
            if 'Date' in indexdict['Pages'][page]:
                li_page_a.tail =  ' '+indexdict['Pages'][page]['Date']
        
    filename = 'web/{}'.format(pagedict['File'])
    write_html_file(page_tree, filename)

        

#####################################################
# Action
#####    
indexdict = { "Pages": {},
              "Cats": {}
              }

site = mwclient.Site(args.host, args.path)
pages=mw_cats(site, args.category) 
indexpage=site.Pages['Index']

# enter pagedict to indexdict in both 'Pages' and 'Cats sub-dictionaries
for category, pages in pages.iteritems():
    for page in pages:
        pagedict = create_page(page)
        pagetitle = pagedict['Title']
        indexdict['Pages'][pagetitle] = pagedict # new entry to indexdict['Pages']
        if pagedict['Type']  == 'Category Page': 
            indexdict['Cats'][pagetitle] = pagedict # add page to indexdict['Cats'] 
            indexdict['Pages'][pagetitle]['Type'] = indexdict['Pages'][pagetitle]['Title'] # modify Category Page's category to its title  

# Category Members
for cat in indexdict['Cats'].keys(): 
    indexdict['Cats'][cat]['MemberPages'] = [] # create placeholder for category MemberPages
for page in indexdict['Pages'].keys(): 
    cat = indexdict['Pages'][page]['Type'] 
    indexdict['Cats'][cat]['MemberPages'].append(page)

templatebase = open("template-base.html", "r")
for page in indexdict['Pages'].keys(): #Fill template. Save static html files
    template_fill(page, templatebase, indexdict)

#pprint.pprint(indexdict, indent=4)

