#! /usr/bin/env python
# -*- coding: utf-8 -*-
import re, subprocess, shlex
import xml.etree.ElementTree as ET

##############################
# MW Categories
##############################
def mw_cats(site, args_cat): # returns pages MEMBER of categories #NO INTERSECTION
    cdict={}
    for cats in args_cat:
        for cname in cats:
            cat = site.Categories[cname]
            pages = list(cat.members())
            cdict[cname]=pages
    catmain = site.Categories["Main"]
    catmain_pages = list(catmain.members())
    cdict["Main"]= catmain_pages    
    # can cat pages be included here???
    return cdict

    
##############################
# CATEGORIES, PAGES AND IMAGES
##############################

def mw_page_cats(site, page):
    cats_list = list(page.categories())
    cats = [cat.name for cat in cats_list if cat.name != u'Category:04 Publish Me'] 
    return cats


def mw_page_imgsurl(page):
    #all the imgs in a page
    #returns list of tuples (img.name, img.fullurl)
    imgs = page.images()
    imgs = list(imgs)   
    imgs_dict = { img.name:(img.imageinfo)['url'] for img in imgs } # exclude thumb 
    imgs_dict = { (key.capitalize()).replace(' ','_'):value for key, value in imgs_dict.items()} # capilatize image name, so it can be called later
    return imgs_dict

##################
# PROCESSING MODULES
##################

def pandoc2html(mw_content):
    '''convert individual mw sections to html'''
    mw_content = mw_content.encode('utf-8')
    tmpfile = open('tmp_content.mw', 'w')
    tmpfile.write(mw_content)
    tmpfile.close()
    args_pandoc = shlex.split( 'pandoc -f mediawiki -t html5 tmp_content.mw' )
    pandoc = subprocess.Popen(args_pandoc, stdout=subprocess.PIPE)
    html = pandoc.communicate()[0]
    html = html.decode("utf-8")
    return html

def write_html_file(html_tree, filename):
    doctype = "<!DOCTYPE HTML>"
    html = doctype + ET.tostring(html_tree,  method='html', encoding='utf-8', ) 
    edited = open(filename, 'w') #write
    edited.write(html)
    edited.close()

cat_exp = re.compile('\[\[Category\:.*?\]\]')
imgfile_exp=re.compile('(File:(.*?)\.(gif|jpg|jpeg|png))')
    
def remove_cats(content):
    content = re.sub(cat_exp, '', content)
    return content
    print 'NO CATS', content

# MAKE SUB more elegant
audio_exp=re.compile('\{\{\#widget:audio\|url\=(.*?)\}\}', re.IGNORECASE) 
video_exp=re.compile('\{\{\#widget:video\|url\=(.*?)\}\}', re.IGNORECASE) 
vimeo_exp=re.compile('\{\{\#widget:vimeo\|(.*?)\}\}', re.IGNORECASE)
youtube_exp=re.compile('\{\{\#widget:youtube\|id\=(.*?)\}\}', re.IGNORECASE)

def replace_video(video):
    video = re.sub(vimeo_exp,"<iframe src='https://player.vimeo.com/video/\g<1>' width='600px' height='450px'> </iframe>", video)
    video = re.sub(youtube_exp, "<iframe src='https://www.youtube.com/embed/\g<1>' width='600px' height='450px'> </iframe>", video)
    video = re.sub(video_exp,"<video src='\g<1>' controls='controls' preload='metadata' type='video/mp4'  width='600px' height='450px'></video>", video)
    video = re.sub(audio_exp,"<audio src='\g<1>' id='audio' controls='controls' preload='metadata'></audio>", video)
    return video
